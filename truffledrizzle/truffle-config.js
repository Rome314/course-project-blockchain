module.exports = {
  contracts_build_directory: "./output/contracts",
  networks: {
    development: {
      host: "ganache-cli",
      port: 8545,
      network_id: "*",
      gas: 4698712,
      gasPrice: 25000000000
    }
  },
  mocha: {
        // timeout: 100000
    },
  solc: {
    optimizer: {
      enabled: true,
      runs: 200
    }
  }
};
